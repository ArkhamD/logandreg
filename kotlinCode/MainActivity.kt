package com.example.logandreg

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.FrameLayout
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.logandreg.databinding.ActivityMainBinding
import com.example.logandreg.ui.login.LoginFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.nav.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.home -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame, HomeFragment.newInstance()).commit()
                }
                R.id.login -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame, LoginFragment()).commit()
                }
                R.id.reg -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frame, RegistrationFragment()).commit()
                }
            }
            true
        }

    }
}



