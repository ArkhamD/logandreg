package com.example.logandreg

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.logandreg.databinding.FragmentHomeBinding
import com.example.logandreg.databinding.FragmentRegistrationBinding
import com.example.logandreg.ui.login.LoginFragment
import java.util.zip.Inflater

class RegistrationFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_registration, container, false)

        val btn = view.findViewById<Button>(R.id.btnToLogin)
        btn.setOnClickListener {
            findNavController().navigate(R.id.action_registrationFragment2_to_loginFragment)
        }

        return view
    }

}